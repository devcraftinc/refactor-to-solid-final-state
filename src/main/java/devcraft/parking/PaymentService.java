package devcraft.parking;


import static java.time.ZoneOffset.UTC;

import java.time.Duration;
import java.time.Instant;


public class PaymentService {

    private final ParkingStore store;
    private Clock clock;
    private PaymentCalculator paymentCalculator = new PaymentCalculator();

    public interface Clock {
        long now();
    }

    public PaymentService(Clock clock, ParkingStore store) {
        this.clock = clock;
        this.store = store;
        paymentCalculator.setEscapeTime(Duration.ofMinutes(10));
    }

    public long enterParking() {
        long entryTime = clock.now();
        return store.addParkingEntry(entryTime);
    }

    public int calcPayment(long code) {
        long entryTime = store.getParkingEntry(code);
        long paymentTime = clock.now();
        return calcPayment(entryTime, paymentTime);
    }

	private int calcPayment(long entryTime, long paymentTime) {
		return paymentCalculator.build().calcPayment(Instant.ofEpochMilli(entryTime), Instant.ofEpochMilli(paymentTime), UTC);
	}

}