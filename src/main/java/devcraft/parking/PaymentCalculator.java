package devcraft.parking;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalTime;

public class PaymentCalculator {
	
	private int weekendDayFixedRate = 40;	
	private Duration escapeTime = Duration.ZERO;

	public void setEscapeTime(Duration escapeTime) {
		this.escapeTime = escapeTime;
	}

	public PaymentPolicy build() {
		PaymentPolicy weeklyPaymentPolicy = makeWeekelyPaymentPolicy();
		return new EscapeTimePaymentPolicy(escapeTime, weeklyPaymentPolicy);
	}

	private WeeklyPaymentPolicy makeWeekelyPaymentPolicy() {
		PaymentPolicy WEEKDAY_PAYMENT_POLICY = new LimitedPaymentPolicy(new HourlyPaymentPolicy());
		PaymentPolicy WEEKEND_POLICY = new LimitedPaymentPolicy(new DayAndNightPaymentPolicy(
				LocalTime.of(22, 0), new HourlyPaymentPolicy(), new FixedPricePolicy(weekendDayFixedRate)));

		WeeklyPaymentPolicy weeklyPaymentPolicy = new WeeklyPaymentPolicy();
		weeklyPaymentPolicy.setPaymentPolicy(DayOfWeek.SUNDAY, WEEKDAY_PAYMENT_POLICY);
		weeklyPaymentPolicy.setPaymentPolicy(DayOfWeek.MONDAY, WEEKDAY_PAYMENT_POLICY);
		weeklyPaymentPolicy.setPaymentPolicy(DayOfWeek.TUESDAY, WEEKDAY_PAYMENT_POLICY);
		weeklyPaymentPolicy.setPaymentPolicy(DayOfWeek.WEDNESDAY, WEEKDAY_PAYMENT_POLICY);
		weeklyPaymentPolicy.setPaymentPolicy(DayOfWeek.THURSDAY, WEEKEND_POLICY);
		weeklyPaymentPolicy.setPaymentPolicy(DayOfWeek.FRIDAY, WEEKEND_POLICY);
		weeklyPaymentPolicy.setPaymentPolicy(DayOfWeek.SATURDAY, WEEKEND_POLICY);
		return weeklyPaymentPolicy;
	}
}