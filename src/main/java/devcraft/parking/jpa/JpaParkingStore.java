package devcraft.parking.jpa;

import devcraft.parking.ParkingStore;

public class JpaParkingStore implements ParkingStore {
    private ParkingEntryRepository repository;

    public JpaParkingStore(ParkingEntryRepository repository) {
        this.repository = repository;
    }

    @Override
    public long addParkingEntry(long entryTime) {
        ParkingEntryRepository.ParkingEntry entry = new ParkingEntryRepository.ParkingEntry();
        entry.setTime(entryTime);
        repository.save(entry);
        return entry.getCode();
    }

    @Override
    public long getParkingEntry(long code) {
        ParkingEntryRepository.ParkingEntry entry = repository.findOne(code);
        return entry.getTime();
    }
}